package ru.tsc.panteleev.tm.api.repository;

import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

}
