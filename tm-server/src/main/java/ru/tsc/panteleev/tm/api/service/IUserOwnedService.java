package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Sort sort);

    @NotNull
    M findById(@NotNull String userId, @NotNull String id);

    @Nullable
    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    long getSize(@NotNull String userId);

    @NotNull
    M changeStatusById(String userId, String id, Status status);

}
