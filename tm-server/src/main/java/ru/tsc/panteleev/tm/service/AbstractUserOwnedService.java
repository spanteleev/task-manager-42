package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.api.service.IConnectionService;
import ru.tsc.panteleev.tm.api.service.IUserOwnedService;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}
