package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;
import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModelDTO> extends IRepository<M> {

    void set(@NotNull Collection<M> models);

    @Nullable
    List<M> findAll();

}
